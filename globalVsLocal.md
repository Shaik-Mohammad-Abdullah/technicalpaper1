## Difference between a globally installed module and a locally installed module?

- A **globally installed module** drops modules in `{prefix}/lib/node_modules`, and puts **executable files** in `{prefix}/bin`, where **{prefix}** is usually something like /`usr/local`.
- A **locally installed module** This installs your package in the current working directory. **Node modules** go in `./node_modules`, **executables** go in `./node_modules/.bin/`, and man pages aren’t installed.

## Resources

- [Difference between globally installed module and locally installed module](https://nodejs.org/uk/blog/npm/npm-1-0-global-vs-local-installation/#:~:text=globally%20%E2%80%94%2D%20This%20drops%20modules%20in,in%20the%20current%20working%20directory.)
