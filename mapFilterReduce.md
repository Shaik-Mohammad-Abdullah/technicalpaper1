# Map, Filter, and Reduce
- **Map method** returns a new array by applying a logic on each element of the array. The Map method has a callback function which takes each element, performs logic and returns out the array.

  **Example code**

  ```javascript
  arr = [1, 2, 3, 4, 5, 6];
  var doubledArr = arr.map((number) => number * 5);
  console.log(doubledArr);
  ```

  **Output**

  ```javascript
  [5, 10, 15, 20, 25, 30];
  ```

  In the above code, Individual elements are passed into the callback function and returns the element(multiplied by 5 times) to a new array.

- **Filter method** returns a new array by applying a logic on each element of the array. The Filter method has a callback function which takes each element, performs logic and returns out the boolean value(true or false).

  **Example code**

  ```javascript
  arr = [1, 2, 3, 4, 5, 6];
  var oddArr = arr.filter((number) => number % 2);
  console.log(oddArr);
  ```

  **Output**

  ```javascript
  [1, 3, 5];
  ```

  In the above code, Individual elements are passed into the callback function and returns the boolean value(true or false) for that element and passes it to a new array.

- **Reduce method** returns a single element by applying a logic on each element of the array. The Reduce method has a callback function which takes each element, performs logic and returns out a desired value.
  **Example code**
  ```javascript
  arr = [1, 2, 3, 4, 5, 6];
  var sum = arr.reduce((accumulater, current) => {
    return accumulater + current;
  }, 0);
  console.log(sum);
  ```
  **Output**
  ```javascript
  21;
  ```
  In the above code, Individual elements are passed into the callback function in the **current** variable and a variable called **accumulator** is assigned with an initial value of 0. Here, the accumulator vaiable will have the previous returned value, whereas the current variable will have the current element of the array.

## Resources
- [Map, Filter and Reduce](https://www.freecodecamp.org/news/javascript-map-reduce-and-filter-explained-with-examples/)