## Why using value === undefined is better than using !value

- **===** strictly checks the value along with the type and **undefined** is returned when we don't return or when we don't assign a value to the variable. Both of these can be used to check the condition of the value.
- !value converts every value to a boolean value which is unreliable in general to check whether the value is meeting certain conditions.

  **Example**

  ```javascript
  value = "3";
  if (value === 3) {
    console.log(`Printing ${value}`);
  }
  ```

  **Output**

  ```javascript

  ```

  In the above code, We obeserve that there is no output because "3" is not equal to 3. Their data types are compared.

  **Example**

  ```javascript
  value = "3";
  if (value) {
    console.log("Print man");
  }
  ```

  **Output**

  ```
  Print man
  ```

  In the above code, We observe that when value is passed to the if consition, The value gets converted into boolean(either 0 or 1) which is unreliable.
