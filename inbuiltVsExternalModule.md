## Difference between an inbuilt module and an external module?

- **Internal modules** are _local_ or _exported members_ of other modules. A name path with more than one identifier is equivalent to a series of nested internal module declarations.

- **External modules** are separately loaded bodies of code referenced using _external module names_. An external module is written as a separate source file that contains at least one import or export declaration.
  - External modules are useful because they hide the internal statements of the module definitions and show only the methods and parameters associated to the declared variable.

## Resources

- [Difference between internal and external modules](https://stackoverflow.com/questions/12841557/whats-the-difference-between-internal-and-external-modules-in-typescript#:~:text=Internal%20modules%20are%20declared%20using,referenced%20using%20external%20module%20names.)
