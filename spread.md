## Spread operator and why it is needed

- Spread syntax (...) allows an iterable data structure such as an array, object, expression or string to be expanded in places where zero or more arguments are expected for funciton calls, or an object expression to be expanded in places where zero or more key-value pairs are expected for onject literals.

  **Example**

  ```javascript
  function sum(v, w, x, y, z) {
    return v + w + x + y + z;
  }

  const numbers = [1, 2, 3, 4, 5];

  console.log(sum(...numbers));
  ```

  **Output**

  ```javascript
  15;
  ```

  In the above code, We observe that the spread operator (numbers) used in the sum function call passes all the indvidual elements of the numbers array to the function declaration block. The arguments of function will take those number of parameters as given by the spread operator.

## Resources

- [Spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
