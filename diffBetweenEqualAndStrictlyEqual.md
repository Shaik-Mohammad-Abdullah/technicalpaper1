## Difference between === and ==

- **==** is used for comparison between two variables irrespective of the datatype of variable.

  **Example**

  ```javascript
  const a = 100;
  const b = "100";
  if (a == b) {
    console.log("Checking value not type");
  }
  ```

  **Output**

  ```javascript
  Checking value not type
  ```

- **===** is used for comparision between two variables but this will check **strict type**, which means it will check datatype and compare two values.

  **Example**

  ```javascript
  const a = 100;
  const b = 100;

  if (a === b) {
    console.log("Checking value with type");
  }
  ```

  **Output**

  ```javascript
  Checking value with type
  ```

## Resources

- [Difference between == and ===](https://www.c-sharpcorner.com/article/difference-between-and-in-javascript2/)
