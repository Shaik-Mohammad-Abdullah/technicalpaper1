## Error handling

- **Error handling** is the set of processes followed to anticipate, detect, resolve errors. There are mainly 3 types of errors that can occur in a JavaScript program.

  - Syntax Errors
  - Runtime Errors
  - Logical Errors

  We solve JavaScript errors using try, catch, finally and throw statements.

  - try{} --> try-block lets you test a bock of code for errors.
  - catch{} --> catch-block lets you handle the errors.
  - throw{} --> throw-block lets you create custom errors.
  - finally{} --> finally-block lets you execute code after try and catch statements regardless of the result.

    **Example code**

  ```javascript
  try {
    console.log("This is the beginning of the try statement");
    const name = "Abdullah";
    call(name);
    console.log(
      "This is the end for the try block which will never run as the program has encountered an error at the top"
    );
  } catch (err) {
    console.log(`Error has occured ${err}`);
  } finally {
    console.log("Finally will always run");
  }
  ```

  **Output**

  ```javascript
  This is the beginning of the try statement
  Error has occured ReferenceError: call is not defined
  Finally will always run
  The Execution continues...
  ```

  In this above code, The **try**-block is testing if there is any error in the code. If it finds one, It sends the error to the **catch**-block which will handle the error. And, Finally it reaches **finally**-block irrespective of the error is solved or not.

- **throw** statement is used to create a new error

  **Example code**

  ```javascript
  let json = "{'age': 30}";
  try {
    let user = JSON.parse(json);
    if (!user.name) {
      throw new SyntaxError("Incomplete data: no name");
    }
    console.log(user.name);
  } catch (err) {
    console.log(`JSON Error ${err}`);
  }
  ```

  **Output**

  ```javascript
  JSON Error SyntaxError: Unexpected token ' in JSON at position 1
  ```

## Importance of Catch-block

- Catch-block statement lets you handle the errors. This block contains statements that specify what to do if an exception is thrown in the try-block.
- If any statement within the try-block throws an exception, control is immediately shifted to the catch-block.
- If no exception is thrown in the try-block, the catch-block is skipped.

> The Example code for catch-block is given in the above code in error handling.

## Resources

- [Error Handling and Creating a new error](https://youtu.be/cFTFtuEQ-10)
